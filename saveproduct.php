<?php
    include('config/db.php');

    $dataBase = new Db();

    $sku = $_POST["sku"];
    $name = $_POST["name"];
    $price = $_POST["price"];
    $description = $_POST["description"];

    $dataBase->addNewProduct($sku, $name, $price, $description);

    echo '<script>window.location = "index.php" </script>';

