<?php
class Db {
    private $myconn="";
    private $servername = "localhost";
    private $username = "id16751996_root";
    private $password = "|7t!v_S9<\?Fuyy@";
    private $database="id16751996_shop";

    public function __construct() {
           $this->myconn = mysqli_connect($this->servername, $this->username,$this->password,$this->database);

           $select = "SELECT * FROM product LIMIT 1";
           $result = $this->myconn->query($select);

            if($result == false)
            {
                $create = "CREATE TABLE product (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                sku VARCHAR(50) NOT NULL,
                name VARCHAR(50) NOT NULL,
                price DECIMAL(10,2) NOT NULL,
                description VARCHAR(50) NOT NULL)
                ";
                $this->myconn->query($create);
           }
    }

    public function addNewProduct($sku, $name, $price, $description) {
        $insert = "INSERT INTO product(sku, name, price, description) VALUES ('$sku','$name', '$price', '$description')";
        
        $this->myconn->query($insert);
    }

    public function getProducts()
    {
        $select = "SELECT * FROM product ORDER BY name";
        return $this->myconn->query($select);
    }

    public function getIds() {
        $select = "SELECT sku FROM product";
        return $this->myconn->query($select);
    }

    public function deleteSelected($del) {
        foreach($del as $id) {
            $delete = "DELETE FROM product WHERE id = $id;";
            $this->myconn->query($delete);
        }
    }

    public function deleteAll() {
        $delete = "DELETE FROM product";
        $this->myconn->query($delete);
    }
}

