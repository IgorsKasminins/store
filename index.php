<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
   </head>
   <body>
   <div class="p-2" style="margin: 30px;">
      <h1 class="float-left">Product List</h1>
      <div>
      <form action="./deleteall.php" method="POST">
        <input type="hidden" id="delete" name="delete">
        <button type="submit" class="btn btn-primary float-right" style="margin-left: 10px;">Mass Delete</button>
        </form>
        <button class="btn btn-outline-primary float-right" onclick="location.href = './addproduct.php';">Add</button>
      </div>
      <br/>
    </div>
    <hr>
   <div class="container">
   <div>
    <form action="./deleteselected.php" method="POST">
    <button type="submit" class="btn btn-primary float-right" style="margin-right: 30px; margin-top: -50px;">Delete Selected</button>
    </div>
    <div class="row" style="margin-top: 80px;">
        <?php
            include('config/db.php');
            $dataBase = new DB();
            $result = $dataBase->getProducts();
            while($row = $result->fetch_assoc()) { ?>
                <div class="card" style="width: 15rem; margin:20px;" align="center">
                <input type="checkbox" style="position:absolute; margin: 20px;" value="<?php echo $row["id"] ?>" name="del[]">
                <label  for="<?php echo $row["id"] ?>"></label>
                <div class="card-body">
                    <h5 class="card-title"><?php echo $row["sku"] ?></h5>
                    <h5 class="card-title"><?php echo $row["name"] ?></h5>
                    <h6 class="card-title"><?php echo $row["price"].' $' ?></h6>  
                    <p class="card-text"><?php echo $row["description"] ?></p>
                </div>
                </div>
            <?php  } ?>
            </form>
        </div>
    </div>
    </body>
</html>
        