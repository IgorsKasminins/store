<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8" />
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
      
      <title>Web Shop</title>
      <script>
      function isNumeric(num){
         if (num == "" || typeof num === 'undefined')
            return -1;
         num = num.replace(',','.');
         return !isNaN(num)
      }

      function validateform(){  
         var name=document.product.name.value;
         var sku=document.product.sku.value;
         var price=document.product.price.value;
         var sel=document.product.description.value;

         <?php   
         include('config/db.php');
         $dataBase = new Db();
         $result = $dataBase->getIds();
         $str = "";
         while($row = $result->fetch_assoc())
            $str = $str.$row["sku"].' ';?>

         var ids = "<?php echo $str; ?>".split(' ');

         console.log(ids);

         for (var i in ids)
         {
            if (ids[i] == sku)
            {
               alert("SKU is not unique. Please, choose another");
               return false;
            }
         }
         if (name == "" || sku == "" || price == "" || sel == "0") {
            alert("Please, submit required data");
            return false;
         } else {
            if(sel == "Size: _ MB") {
               var description=document.product.size.value;
               if(isNumeric(description) === false)
               {
                  alert("Size contains unvalid values"); 
                  return false;
               }else if(isNumeric(description) === -1) {
                  alert("Please, submit required data for Size");
                  return false;
               }
            } else if(sel == "Weight: _ KG"){
               var description=document.product.weight.value;
               if(isNumeric(description) === false)
               {
                  alert("Weight contains unvalid values"); 
                  return false;
               }else if(isNumeric(description) === -1) {
                  alert("Please, submit required data for Weight");
                  return false;
               }

            } else if(sel == "Dimension: _x_x_") {
               var description=document.product.height.value;
               if(isNumeric(description) === false)
               {
                  alert("Height contains unvalid values"); 
                  return false;
               }else if(isNumeric(description) === -1) {
                  alert("Please, submit required data for Height");
                  return false;
               }

               description=document.product.width.value;
               if(isNumeric(description) === false)
               {
                  alert("Width contains unvalid values"); 
                  return false;
               }else if(isNumeric(description) === -1) {
                  alert("Please, submit required data for Width");
                  return false;
               }

               description=document.product.length.value;
               if(isNumeric(description) === false)
               {
                  alert("Length contains unvalid values"); 
                  return false;
               }else if(isNumeric(description) === -1) {
                  alert("Please, submit required data for Length");
                  return false;
               }
            }
         }

            //selected types have its own template of a description
            //which gets replaced by according values
            var template = document.getElementById('description').value;
            var typeInputs = document.getElementsByClassName('description');

            //inputs
            var valueInputs = [];
            var error = "";
            
            for(var i in typeInputs){
               var res = isNumeric(typeInputs[i].value);
               if(res === true)
                  valueInputs.push(typeInputs[i].value);
            }
            //'places' of a template to be replaced by inputs
            var position = template.indexOf('_');

            while(position != '-1')
            {
               for (var i in valueInputs)
                  template = template.replace('_', parseFloat(valueInputs[i]).toFixed(2));

               position = template.indexOf('_');
            }

            //result is located in a hidden input
            document.getElementById('description').value = template;
         


      }
      //after changing a type all specific values get reset
      //and a new template gets set
      function updateSelected(template)
      {
         if (document.getElementById('description').value == template) {
            document.getElementById('description').value = '';
            return;
         }

         var values = document.getElementsByClassName('description');

         for (var i in values)
            values[i].value = '';

         document.getElementById('description').value = template;
      }

      </script>  
   </head>
   <body>
   <div class="p-2" style="margin: 30px;">
      <h1 class="float-left">Add a product</h1>
      <div>
         <button class="btn btn-outline-primary float-right" onclick="location.href = './index.php';">Cancel</button>
         <form name="product" action="saveproduct.php" method="post" onsubmit="return validateform()">
         <button type="submit" class="btn btn-primary float-right" style="margin-right: 10px;">Save</button>
      </div>
      <br/>
    </div>
    <hr>
      <div class="container" style="width: 500px;">
            <div class="form-group">
               <label for="sku">SKU</label>
               <input type="text" class="form-control" id="sku" name = "sku" aria-describedby="emailHelp" placeholder="SKU" min = "0" required>
            </div>
            <div class="form-group">
               <label for="name">Name</label>
               <input type="text" class="form-control" id="name" name = "name" placeholder="Name" required>
            </div>
            <div class="form-group">
               <label for="price">Price ($)</label>
               <input type="number" class="form-control" id="price" name = "price" placeholder="Price" step="0.01" min = "0.01" required>
            </div>
            <div id="accordion">
               <div class="card">
                  <div class="card-header" id="dvd">
                     <h5 class="mb-0">
                     <button type="button" onclick="updateSelected('Size: _ MB')" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        DVD
                     </button>
                     </h5>
                  </div>
                  <div id="collapseOne" class="collapse" aria-labelledby="dvd" data-parent="#accordion">
                     <div class="card-body">
                        <div class="form-group">
                           <label for="name">Size (MB)</label>
                           <input type="number" class="form-control description" id="size" name = "size" placeholder="Size" step="0.01" min = "0.01">
                        </div>
                    </div>
                  </div>
               </div>
               <div class="card">
                  <div class="card-header" id="book">
                     <h5 class="mb-0">
                     <button type="button" onclick="updateSelected('Weight: _ KG')" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Book
                     </button>
                     </h5>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="book" data-parent="#accordion">
                     <div class="card-body">
                        <div class="form-group">
                           <label for="weight">Weight (Kg)</label>
                           <input type="number" class="form-control description" id="weight" name = "weight" placeholder="Weight" step="0.01" min = "0.01">
                        </div>
                    </div>
                  </div>
               </div>
               <div class="card">
                  <div class="card-header" id="furniture">
                     <h5 class="mb-0">
                     <button type="button" onclick="updateSelected('Dimension: _x_x_')" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        Furniture
                     </button>
                     </h5>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="furniture" data-parent="#accordion">
                     <div class="card-body">

                     <label for="height">Height (CM)</label>
                     <input type="number" class="form-control description" id="height" name = "height" placeholder="Height" step="0.01" min = "0.01">
                        
                     <label for="width">Width (CM)</label>
                     <input type="number" class="form-control description" id="width" name = "width" placeholder="Width" step="0.01" min = "0.01">

                     <label for="length">Lenght (CM)</label>
                     <input type="number" class="form-control description" id="length" name = "length" placeholder="Lenght" step="0.01" min = "0.01">
                     </div>
                  </div>
               </div>
               </div>
               <input type="hidden" id="description" name="description" value="0">
         </form>
      </div>
   </body>
</html>

